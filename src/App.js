import './styles/flex-test.css';
import {FlexTest} from  './components/flex-test.js';

// https://place-hold.it/300x500
function App() {
    return (
        <div className="app-container">
            <FlexTest/>
        </div>
    );
}

export default App;
