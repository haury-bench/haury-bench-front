import '../styles/flex-test.css';

// https://place-hold.it/300x500
export function FlexTest() {
    return (
        <div className="page">
            <div className="header">
                <a href={"#"}>Acceuil</a>
                <a href={"#"}>Contact</a>
                <a href={"#"}>Logout</a>
                <a href={"#"}>Logout</a>
                <a href={"#"}>Logout</a>
                <a href={"#"}>Logout</a>
                <a href={"#"}>Logout</a>
            </div>

            <div className="sidebar">
                je suis une side bar
            </div>

            <div className="body">
                <div className={"mosaic"}>
                    <img src="https://place-hold.it/200x150 " alt="basic image" className="img-item-1"/>
                    <img src="https://place-hold.it/200x50 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x150 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x65 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x100 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x150 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x65 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x150 " alt="basic image" className="img-item-1"/>
                    <img src="https://place-hold.it/200x50 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x150 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x65 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x100 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x150 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x65 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x150 " alt="basic image" className="img-item-1"/>
                    <img src="https://place-hold.it/200x50 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x150 " alt="basic image" className="img-item-2"/>
                    <img src="https://place-hold.it/200x65 " alt="basic image" className="img-item-2"/>
                </div>
            </div>
        </div>
    );
}
